const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by the other files.
module.exports = mongoose.model('Task', taskSchema)

