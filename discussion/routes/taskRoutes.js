//for routes handling

const express = require('express');
const taskConroller = require('../controllers/taskConroller.js')

// Creates a router instance that functions as a middleware anf routing system.
const router = express.Router()

//Routes

router.get('/',(req, res) => {
	taskConroller.getAllTasks().then((resultFromController) => res.send(resultFromController))
})

// Route for creating task - runs the createTask function fro the controller
router.post('/create', (req, res) => {
	taskConroller.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/update',(req, res) =>{
	taskConroller.updateTask(req.params.id, req.body).then ((resultFromController) => res.send(resultFromController))
})

router.get('/tasks/:id',(req, res) => {
	taskConroller.getSpecificTasks().then((resultFromController) => res.send(resultFromController))
})

router.put('/tasks/:id',(req, res) =>{
	taskConroller.updateSpecificTask(req.params.id, req.body).then ((resultFromController) => res.send(resultFromController))
});


module.exports = router